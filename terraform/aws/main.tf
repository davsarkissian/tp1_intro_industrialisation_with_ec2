


resource "aws_key_pair" "deployer" {
  key_name   = "davidsshcloud"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDEa0HWNkOlR/pFCnucDy9XHcxXq+uxDtVRMIfSZXrG9n1jOAFlfpXSgknR7G/GZOZv8KB/0SkKVXUox56SLKfCPRsZw+B4xjsUgfbgB56snEDjKnhs7L2TIiFma+/Xt4/5E5HgidlNSeRxTfnJ6guyV/gEidPe1xVk2tGyvDYjmPt3dgsaKnsbsUZqUAmK3tb2mqNLMhHRfzNAx9wn13IIKEBl0Py1FWSg72pIIyIwNAku2yqAoxABD2l+gvNQep0EVPUjSHdB9QsEGZ2kADkEz1U88VaEwx2Pwe0oITxR4U9NdKVb7FRpfM2OdTV1GfMYm4eDyadyWKzPJfl40rk1i4J//hF0CJ90ET4Wl6YpPOFuLCegOB4FLP70VIcH+EMAO8MIYR4hSx0mGV891xiCTeM3jrpo4CPgtHPV3AbLC+gn2evWMpKHLViiiYdc1Wdv/NCiFN0maTXrzBiHlZpnzCGpbfKjbPHkO21IUG4ifjoGZT+pZo096cBhZ4QBMrE= david@LAPTOP-KD7CF6RN"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}


resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress                = [
    {
      cidr_blocks      = [ "0.0.0.0/0", ]
      description      = ""
      from_port        = 22
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = 22
    }
  ]
}



resource "aws_instance" "web" {
  ami           = "ami-02df9ea15c1778c9c"
  instance_type = "t2.micro"
  key_name = "davidsshcloud"
  tags = {
    Name = "esme"
  }
}